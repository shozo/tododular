import { Feather } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import HomeScreen from '../screens/HomeScreen';

import { BottomTabParamList, HomeParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

const BottomTabNavigator = () => (
  <BottomTab.Navigator initialRouteName='Home'>
    <BottomTab.Screen
      name='Home'
      component={HomeScreen}
      options={{
        tabBarIcon: () => <TabBarIcon name='home' color='black' />,
      }}
    />
  </BottomTab.Navigator>
);

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
const TabBarIcon = ({ name, color }: { name: string; color: string }) => (
  <Feather size={25} name={name} color={color} />
);

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const HomeStack = createStackNavigator<HomeParamList>();

export const HomeScreenNavigator = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen
      name='HomeScreen'
      component={HomeScreen}
      options={{ headerTitle: 'Tab One Title' }}
    />
  </HomeStack.Navigator>
);

export default BottomTabNavigator;
