export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Home: undefined;
};

export type HomeParamList = {
  HomeScreen: undefined;
};
